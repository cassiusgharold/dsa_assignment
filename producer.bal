import ballerina/io;
import ballerina/uuid;
import ballerinax/kafka;

kafka:ProducerConfiguration producerConfigs = {
    clientId: "dsa.assignment02",
    //Number of acknowledgments
    acks: "all",
    retryCount: 3,
    //Exactly one copy of each message
    enableIdempotence: true,
    transactionalId: "test-transactional-id",
    //files have a limited size of 30 MB
    sendBuffer: 30
};

// remote server endpoints of Kafka brokers
final string DEFAULT_URL = "localhost:9092";

kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL, producerConfigs);

public function main() {
    json message = {
        message: "In this example, a WebSub Subscriber service is used to implement a GitHub-based WebHook service."
    };

    byte[] serializedMessage = message.toBalString().toBytes();

    kafkaAdvancedTransactionalProduce(serializedMessage);

}

function kafkaAdvancedTransactionalProduce(byte[] message) {
    transaction {
        kafka:Error? sendResult = kafkaProducer->send({
            topic: "dsa.assignment02",
            value: message,
            'key: uuid:createType1AsString().toBytes()
        });

        //Checks for an error and notifies if an error has occurred.
        if sendResult is kafka:Error {
            io:println("Error occurred when sending message ", sendResult);
        }

        var commitResult = commit;
        if commitResult is () {
            io:println("Transaction successful");
        } else {
            io:println("Transaction unsuccessful " + commitResult.message());
        }
    }
}

